const priceBox = document.querySelector(".price_box");
const input = document.getElementById("price");

input.addEventListener("focus", () => {
    input.style.borderColor = "green";
});
input.addEventListener("blur", () => {
    input.style.borderColor = "#333";
    let price = parseFloat(input.value);

function closeButton (button) {
        button.addEventListener("click", () => {
            input.value = "";
            document.querySelector(".message-box").remove();
            input.style.color = "black";
        })
}

    if (price < 0 || isNaN(price)) {
        priceBox.style.cssText = "background-color: red;"
        priceBox.insertAdjacentHTML("beforeend", `<div class = "message-box"><span class = "message">Please enter correct price</span><button class="button" type="button">X</button></div>`)
        const button = document.querySelector(".button");
        closeButton(button);

    } else {
        priceBox.insertAdjacentHTML("afterbegin", `<div class = "message-box"><span class = "message">Current price is ${price}</span><button class="button" type="button">X</button></div>`);
        input.style.color = "green";

        const button = document.querySelector(".button");
        closeButton(button);
    }
    
})

