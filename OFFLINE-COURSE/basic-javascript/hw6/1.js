const filterBy = (array, type) => {
    return array.filter((element) => {
        return typeof element !== type;
    });
};

console.log(filterBy([{ a: 42, b: "rag" }, { grd: "ds" }, 56, 13, "plum"], "number"));
