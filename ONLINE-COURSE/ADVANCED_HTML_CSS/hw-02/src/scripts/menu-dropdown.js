const menuBatton = document.querySelector(".header__burger-icon");

const dropdowmMenu = document.querySelector(".header__dropdown-menu");
const dropdowmItemCol = document.querySelectorAll(".header__dropdown-item")

const mainMenu = document.querySelector(".header__main-menu")
const menuItemCol = document.querySelectorAll(".header__menu-item")

menuBatton.addEventListener("click", () => {
    dropdowmMenu.classList.toggle("active");
    menuBatton.classList.toggle("active")
})


dropdowmMenu.addEventListener("click", (event) => {
    if (event.target.classList.contains("header__dropdown-item")) {
        event.preventDefault();
        document.querySelector(".header__dropdown-item.active")?.classList.remove("active")
        event.target.classList.add("active");
    }
})

mainMenu.addEventListener("click", (event) => {
    if (event.target.classList.contains("header__menu-item")) {
        event.preventDefault();
        document.querySelector(".header__menu-item.active").classList.remove("active")
        event.target.classList.add("active");
    }
})