import gulp from "gulp";
import clean from "gulp-clean";
import dartSass from "sass";
import gulpSass from "gulp-sass";
import concat from "gulp-concat";
import cleanCss from "gulp-clean-css";
import autoPrefixer from "gulp-autoprefixer";
import terser from "gulp-terser";
import imagemin from "gulp-imagemin";
import browserSync from "browser-sync";

const sass = gulpSass(dartSass);
const bs = browserSync.create()

const cleanDist = () => gulp.src("./dist/*", {"read": false})
.pipe(clean());

const buildCSS = () => gulp.src("./src/styles/**/*")
.pipe(sass())
.pipe(concat("styles.min.css"))
.pipe(cleanCss())
.pipe(autoPrefixer())
.pipe(gulp.dest("./dist/"));

const buildJS = () => gulp.src("./src/scripts/**/*")
.pipe(concat("script.min.js"))
.pipe(terser())
.pipe(gulp.dest("./dist/"))

const imgMinify = () => gulp.src("./src/images/**/*")
.pipe(imagemin())
.pipe(gulp.dest("./dist/images/"));

export const build = gulp.series(cleanDist, gulp.parallel(buildCSS, buildJS, imgMinify));

export const dev = () => {
    bs.init({
        server: {
            baseDir: "./"
        }
    });

    gulp.watch("./src/**/*", gulp.series(gulp.parallel(buildCSS, buildJS), (done) => {
        bs.reload();
        done()
    }))
}