// ex 1

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const clientsSet = new Set([...clients1, clients2]);
const clientsArr = [...clientsSet]
console.log("EX 1: ", clientsArr);

// ex 2
const characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Кэролайн",
        lastName: "Форбс",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Аларик",
        lastName: "Зальцман",
        age: 31,
        gender: "man",
        status: "human"
    },
    {
        name: "Дэймон",
        lastName: "Сальваторе",
        age: 156,
        gender: "man",
        status: "vampire"
    },
    {
        name: "Ребекка",
        lastName: "Майклсон",
        age: 1089,
        gender: "woman",
        status: "vempire"
    },
    {
        name: "Клаус",
        lastName: "Майклсон",
        age: 1093,
        gender: "man",
        status: "vampire"
    }
];

const charactersShortInfo = characters.map(({ name, lastName, age }) => {
    return {
        name,
        lastName,
        age,
    }
})

console.log("EX 2: ", charactersShortInfo);

// ex 3

const user1 = {
    name: "John",
    years: 30
  };

let {name, years:userAge, isAdmin = false} = user1

console.log(`EX 3: name: ${name}, age: ${userAge}, isAdmin: ${isAdmin}`);

// ex 4

const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
      lat: 38.869422, 
      lng: 139.876632
    }
  }
  
  const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
  }
  
  const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto', 
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
  }

  const satoshiFull = {...satoshi2018, ...satoshi2019, ...satoshi2020}
  console.log("EX 4: ", satoshiFull);

//   ex 5
const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
  }, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
  }, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
  }];
  
  const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
  }

  const newBooks = [...books, bookToAdd]
  console.log("EX 5: ",newBooks);

//   ex 6
const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
  }

const newEmpoyee = {
    ...employee,
    age: 45,
    salary: 2500
}
console.log("EX 6: ", newEmpoyee);

// ex 7
const array = ['value', () => 'showValue'];

const [value, showValue] = array;

alert(`EX 7: ${value}`);
alert(showValue()); 