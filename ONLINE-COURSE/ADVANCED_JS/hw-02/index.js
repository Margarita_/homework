const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 370
    },
    {
        author: "Сюзанна Кларк",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 370
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 270
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 400
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

class LackOfDataError extends Error {
    constructor(values) {
        super()
        this.name = "Lack of data";
        this.message = `You need to indicate next data: ${values.join(", ")}`
    }
}

class BookListItem {
    constructor(author, name, price) {
        if (author === undefined || name === undefined || price === undefined) {
            const undefValues = []
            if (author === undefined) {
                undefValues.push("author")
            }
            if (name === undefined) {
                undefValues.push("name")
            }
            if (price === undefined) {
                undefValues.push("price")
            }

            throw new LackOfDataError(undefValues)
        }

        this.name = name;
        this.author = author;
        this.price = price;
        this.li = document.createElement("li")
    }

    render(container) {
        this.li.innerText = `"${this.name}", автор ${this.author}, ціна: ${this.price} UAH`
        container.append(this.li)
    }
}

bookList = document.createElement("ul")
document.querySelector(".root").append(bookList)

  books.forEach(el => {
    try {
        new BookListItem(el.author, el.name, el.price).render(bookList)
    } catch (error) {
        if (error instanceof LackOfDataError) {
            console.warn(error)
        } else {
            throw error
        }
    }
  })
