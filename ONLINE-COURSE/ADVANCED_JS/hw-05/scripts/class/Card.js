import deletePost from "../api/deletePost.js"

class Card {
    constructor(postId, name, email, title, text, photo = "./images/photo-placeholder.jpg") {
        this.postId = postId,
        this.name = name,
        this.email = email,
        this.title = title,
        this.text = text,
        this.photo = photo,
        this.cardContainer = document.createElement("div")
    }

    render(container){
        this.cardContainer.classList.add("card")
        this.cardContainer.id = this.postId;
        this.cardContainer.insertAdjacentHTML("beforeend", `
            <div class="card-image">
                <img src="${this.photo}" alt="">
            </div>
            <div class="card-body">
                <div class="title-wrap">
                    <div>
                        <p class="user-info">${this.name} <a href="mailto:${this.email}">${this.email}</a></p>
                        <h3 class="title">${this.title}</h3>
                    </div>
                    <button class="button">Delete</button>
                </div>
                <p class="text">${this.text}</p>
            </div>`)
        this.cardContainer.querySelector(".button").addEventListener("click", deletePost)

        container.append(this.cardContainer)
    }
}

export default Card