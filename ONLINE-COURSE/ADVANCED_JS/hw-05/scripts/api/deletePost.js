const deletePost = ({ target }) => {
    fetch(`https://ajax.test-danit.com/api/json/posts/${target.closest(".card").id}`, {
        method: "DELETE",
    })
    .then(res => {
        if (res.ok) {
            document.getElementById(`${target.closest(".card").id}`).remove()
        } else {
            throw new Error
        }
    })
    .catch(error => {
        console.warn(error);
    })
}

export default deletePost