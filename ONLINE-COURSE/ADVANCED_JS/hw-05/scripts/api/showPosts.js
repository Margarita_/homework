import Card from "../class/Card.js"

const postUrl = "https://ajax.test-danit.com/api/json/posts"
const userUrl = "https://ajax.test-danit.com/api/json/users"

const showPosts = () => {
    fetch(postUrl).then(res => res.json())
    .then(posts => {
        fetch(userUrl).then(res => res.json())
        .then(users => {
            posts.forEach(({title, body, userId, id:postId}) => {
                const {name, email} = users.find(el => el.id === userId)
                new Card (postId, name, email, title, body).render(document.querySelector(".card-wrap"))
            });
        })
        .catch(error => {
            console.warn(error);
        })
    })
    .catch(error => {
        console.warn(error);
    })
}

export default showPosts