const getIP = async () => {
  try {
    const response = await fetch("https://api.ipify.org/?format=json")
      return await response.json()
  } catch (error) {
    console.warn(error);
  }
}

const getAdress = async () => {
  const {ip} = await getIP();
  try {
    const [{continent, country, city, regionName}] = await fetch("http://ip-api.com/batch", {
    method: "POST",
    mode: "cors",
    body: JSON.stringify([{
      query: ip,
      fields: "status,message,continent,country,regionName,city,district"
    }])
  }).then(res => res.json())
    return `Adress: ${continent}, ${country} ${regionName}, ${city}`
  } catch (error) {
    console.warn(error);
  }
  
}

const renderAdress = async (container) => {
  container.innerText = await getAdress()
}

document.querySelector(".button").addEventListener("click", async ()=> {
  document.querySelector(".text").innerText = "Loading...";
  await renderAdress(document.querySelector(".text"))
})

console.log("F");