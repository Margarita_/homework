class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name () {
        if (!confirm("Are you a robot?")) {
            return this._name
        } else {
            alert("Sorry, access denied")
        }
    }

    set name (name) {
        if (prompt("Enter your password") === "12345") {
            this._name = name
        } else {
            alert("Sorry, access denied")
        }
    }

    get age () {
        if (!confirm("Are you a robot?")) {
            return this._age
        } else {
            alert("Sorry, access denied")
        }
    }

    set age (age) {
        if (prompt("Enter your password") === "12345") {
            this._age = age
        } else {
            alert("Sorry, access denied")
        }
    }

    get salary () {
        if (!confirm("Are you a robot?")) {
            return this._salary
        } else {
            alert("Sorry, access denied")
        }
    }

    set salary (salary) {
        if (prompt("Enter your password") === "12345") {
            this._salary = salary
        } else {
            alert("Sorry, access denied")
        }
    }
}

class Programmer extends Employee {
    constructor(lang, name, age, salary) {
        super(name, age, salary);
        this._lang = lang
    }
    get salary () {
        return super.salary*3;
    }

    get lang () { if (!confirm("Are you a robot?")) {
        return this._lang
    } else {
        alert("Sorry, access denied")
    }
    }

    set lang (lang) {
        if (prompt("Enter your password") === "12345") {
            this._lang = lang
        } else {
            alert("Sorry, access denied")
        }
    }

}

const david = new Programmer("JS", "David", 25, 2500)
const inna = new Programmer("Java", "Inna", 31, 2000)
const olena = new Programmer("PHP", "Olena", 35, 3200)

console.log(david, inna, olena);

