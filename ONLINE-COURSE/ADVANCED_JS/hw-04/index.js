const filmsUrl = "https://ajax.test-danijjt.com/api/swapi/films";
const cardWrap = document.querySelector(".cards__card-wrap");

class FilmCard {
  constructor(name, episodeId, text) {
    this.name = name,
    this.episodeId = episodeId,
    this.text = text,
    this.cardContainer = document.createElement("div")
  }

  render(container){
    this.cardContainer.classList.add("cards__card-item");
    this.cardContainer.innerHTML = `<h3>Episode ${this.episodeId}. ${this.name}</h3><p>${this.text}</p><ul class = "cards__char-list">Characters:</ul>`

    container.append(this.cardContainer)
  }
}

fetch(filmsUrl).then(response => response.json())
.then(data => {
  data.forEach(({name, episodeId, openingCrawl}) => {
    const filmCard = new FilmCard(name, episodeId, openingCrawl);
    filmCard.render(cardWrap);
  });
return data
})
.then(data => {
  const listCollection = document.querySelectorAll(".cards__char-list");

  data.forEach(({characters}, index) => {
    const promiseArr = characters.map(url => fetch(url).then(res => res.json()))
    Promise.allSettled(promiseArr)
      .then(result => {
        console.log(result);
        result.forEach(({status, value:{name}}) => {
          if (status === "fulfilled") {
            listCollection[index].insertAdjacentHTML("beforeend", `<li>${name}</li>`)
          }
        })
      })
  })
})
.catch(error => console.warn(error))
