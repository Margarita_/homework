
const tabCollection = document.querySelectorAll(".tabs-title")
const tabContentCollection = document.querySelectorAll(".tabs-content li")

const changeTabs = (event) => {
    tabCollection.forEach((element) => {
        element.classList.remove("active")
    })

    event.target.classList.add("active")

    tabContentCollection.forEach((element) => {
       if (element.dataset.title === event.target.dataset.title) {
           element.classList.add("active")
       } else element.classList.remove("active")
    })

}


tabCollection.forEach((element) => {
    element.addEventListener("click", changeTabs)
})