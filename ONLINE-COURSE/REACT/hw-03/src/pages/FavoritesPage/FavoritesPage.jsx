import FavoriteItem from "../../components/FavoriteItem/FavoriteItem"
import style from "./FavoritesPage.module.scss"
import { useEffect } from "react";
import PropTypes from "prop-types"

const FavoritesPage = ({addToCart, setFavorites, products, setProducts, favorites, setIsOpenModal, setModalProps }) => {


    useEffect(() => {
        setProducts((current) => {
            const newProducts = [...current]
            newProducts.forEach(el => {
                if (favorites.indexOf(el.id) > -1) {
                    el.isFavorite = true;
                } else {
                    el.isFavorite = false
                }
            })
            return newProducts
        })
    }, [favorites])

    return (
        <div className={style.container}>
            {products.map(product => {
                if (product.isFavorite) {

                    return (<FavoriteItem
                        key={product.id}
                        title={product.title}
                        img={product.img}
                        id={product.id}
                        price={product.price}
                        color={product.color}
                        isFavorite={product.isFavorite}
                        setFavorites={setFavorites}
                        setIsOpenModal={setIsOpenModal}
                        setModalProps={setModalProps}
                        addToCart={addToCart}
                    />)
                }
            })}

        </div>
    )
}

FavoritesPage.propTypes = {
    products : PropTypes.arrayOf(PropTypes.shape({
        title : PropTypes.string.isRequired, 
        img: PropTypes.string.isRequired, 
        id: PropTypes.string.isRequired, 
        price: PropTypes.number.isRequired, 
        color: PropTypes.string, 
        isFavorite: PropTypes.bool
    })).isRequired,
    favorites : PropTypes.arrayOf(PropTypes.string).isRequired,
    setProducts: PropTypes.func,
    addToCart : PropTypes.func,
    setFavorites: PropTypes.func,
    setIsOpenModal : PropTypes.func,
    setModalProps : PropTypes.func
}

FavoritesPage.defaultProps = {
    setProducts: () => {},
    addToCart : () => {},
    setFavorites: () => {},
    setIsOpenModal : () => {},
    setModalProps : () => {},
}

export default FavoritesPage