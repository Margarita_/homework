import CartItem from "../../components/CartItem/CartItem"
import style from "./CartPage.module.scss"
import PropTypes from "prop-types"

const CartPage = ({cart, setCart, setIsOpenModal, setModalProps}) => {
    return (
    <section className={style.container}>
            {cart.map(el => {
            return (
                <CartItem key={`${el.id} ${Math.random()}`}
                title={el.title}
                img={el.img}
                id={el.id}
                price={el.price}
                color={el.id}

                setCart={setCart}
                setIsOpenModal={setIsOpenModal}
                setModalProps={setModalProps}
                />)
            })}
     </section>
    )
}

CartPage.propTypes = {
    cart : PropTypes.arrayOf(PropTypes.shape({
        title : PropTypes.string.isRequired, 
        img: PropTypes.string.isRequired, 
        id: PropTypes.string.isRequired, 
        price: PropTypes.number.isRequired, 
        color: PropTypes.string, 
        isFavorite: PropTypes.bool
    })).isRequired,
    setCart : PropTypes.func,
    setIsOpenModal : PropTypes.func,
    setModalProps : PropTypes.func
}

CartPage.defaultProps = {
    setCart: () => {}, 
    setIsOpenModal: () => {}, 
    setModalProps: () => {}
}

export default CartPage