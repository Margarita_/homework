import { useEffect, useState } from 'react';
import '../../App.module.scss';
import CardList from '../../components/CardList/CardList';
import PropTypes from "prop-types"

const HomePage = ({ addToCart, setIsOpenModal, setModalProps, favorites, setFavorites, products, setProducts }) => {
    

    const toggleFavorites = (id) => {
        setFavorites((current) => {
            const index = current.indexOf(id)
            const newFav = [...current]
            if (index > -1) {
                newFav.splice(index, 1)
            } else {
                newFav.push(id)
            }
            localStorage.setItem("favorites", JSON.stringify(newFav))
            return newFav
        })

    }

    useEffect(() => {
        setProducts((current) => {
            const newProducts = [...current]
            newProducts.forEach(el => {
                if (favorites.indexOf(el.id) > -1) {
                    el.isFavorite = true;
                } else {
                    el.isFavorite = false
                }
            })
            return newProducts
        })
    }, [favorites])


    return (
        <>
            <CardList
                cardArray={products}
                addToCart={addToCart}
                addToFavorites={toggleFavorites}
                setIsOpenModal={setIsOpenModal}
                setModalProps={setModalProps} />
        </>
    );

}

HomePage.propTypes = {
    products : PropTypes.arrayOf(PropTypes.shape({
        title : PropTypes.string.isRequired, 
        img: PropTypes.string.isRequired, 
        id: PropTypes.string.isRequired, 
        price: PropTypes.number.isRequired, 
        color: PropTypes.string, 
        isFavorite: PropTypes.bool
    })).isRequired,
    favorites : PropTypes.arrayOf(PropTypes.string).isRequired,
    setProducts: PropTypes.func,
    addToCart : PropTypes.func,
    setFavorites: PropTypes.func,
    setIsOpenModal : PropTypes.func,
    setModalProps : PropTypes.func
}

HomePage.defaultProps = {
    setProducts: () => {},
    addToCart : () => {},
    setFavorites: () => {},
    setIsOpenModal : () => {},
    setModalProps : () => {},
}

export default HomePage