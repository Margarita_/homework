import { Route, Routes } from "react-router-dom"
import CartPage from "./pages/CartPage/CartPage"
import FavoritesPage from "./pages/FavoritesPage/FavoritesPage"
import HomePage from "./pages/HomePage/HomePage"

const AppRoutes = ({cart, setCart, addToCart, setIsOpenModal, setModalProps, favorites, setFavorites, products, setProducts}) => {
    return (
        <Routes>
            <Route path="/" element={ <HomePage products={products} setProducts={setProducts} favorites={favorites} setFavorites={setFavorites} setIsOpenModal={setIsOpenModal} setModalProps={setModalProps} cart={cart} addToCart={addToCart}/> } />
            <Route path="/cart" element={<CartPage cart={cart} setCart={setCart} setIsOpenModal={setIsOpenModal} setModalProps={setModalProps}/>} />
            <Route path="/favorites" element={<FavoritesPage addToCart={addToCart} setFavorites={setFavorites} products={products} setIsOpenModal={setIsOpenModal} setModalProps={setModalProps} favorites={favorites} setProducts={setProducts}/>} />
        </Routes>
    )

}

export default AppRoutes