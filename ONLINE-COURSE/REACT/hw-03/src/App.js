import { useEffect, useState } from 'react';
import './App.module.scss';
import Header from "./components/Header/Header"
import Modal from "./components/Modal/Modal"
import AppRoutes from "./AppRoutes"

const App = () => {
  const [cart, setCart] = useState([]);
  const [favorites, setFavorites] = useState([]);
  const [products, setProducts] = useState([]);
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [modalProps, setModalProps] = useState({});

  const addToCart = (card) => {
    setCart((current) => {
        const newCart = [...current, card]
        localStorage.setItem("cart", JSON.stringify(newCart))
        return newCart
    })

}

  useEffect(() => {
    if (localStorage.getItem("cart")){setCart(JSON.parse(localStorage.getItem("cart")))}
    if (localStorage.getItem("favorites")){setFavorites(JSON.parse(localStorage.getItem("favorites")))}
    const getProducts = async () => {
      try {
          const products = await fetch("./products.json").then(res => res.json())
          products.forEach(el => {
              if (localStorage.getItem("favorites").indexOf(el.id) > -1) {
                  el.isFavorite = true;
              }
          })
          setProducts(products)

      } catch (error) {
          console.warn(`CATCH: ${error}`);
      }
  }
  getProducts()
  }, [])

  return (
    <div className="App">
      <Header cartCount={cart.length} favoriteCount={favorites.length}/>
      <AppRoutes 
      favorites={favorites} 
      setFavorites={setFavorites} 
      cart={cart} addToCart={addToCart}
      setCart={setCart} 
      setIsOpenModal={setIsOpenModal} 
      setModalProps={setModalProps}
      products={products}
      setProducts={setProducts}/>
      {isOpenModal && <Modal
                modalProps={modalProps}
                setIsOpenModal={setIsOpenModal} />}
    </div>
  )
}

export default App;
