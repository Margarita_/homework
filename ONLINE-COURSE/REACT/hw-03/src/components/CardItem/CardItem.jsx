import PropTypes from "prop-types";
import style from "./CardItem.module.scss"
import starFilled from "./star-filled.svg"
import starOutline from "./star-outline.svg"
import Button from "../Button/Button"

const CardItem = ({ title, img, id, price, color, isFavorite, addToCart, addToFavorites, setIsOpenModal, setModalProps }) => {
        return (
            <div className={style.cardWrap}>
                <img className={style.productImg} src={img} alt={title} />

                <button className={style.favoriteButton}
                        onClick={() => {addToFavorites(id)}}>
                    <img className={style.favoriteIcon} src={isFavorite ? starFilled : starOutline} alt="" />
                </button>

                <p className={style.title}>{title}</p>
                <p className={style.price}>{price} &#8372;</p>
                <div className={style.color}>
                    <p>Color:</p>
                    <div style={{backgroundColor: color}}></div>
                </div>
                <p className={style.id}>Code: {id}</p>

                <Button 
                text="Add to cart" 
                backgroundColor="#123456" 
                onClick={() => {
                    setModalProps({
                        header: "Add to cart", 
                        text: `Are you sure you want add to cart ${title}?`, 
                        onSubmit: ()=>{
                            addToCart({title, img, id, price, color, isFavorite})
                            setIsOpenModal(false);
                        }
                    });
                    setIsOpenModal(true);
                }}/>
            </div>
        )
    }

CardItem.propTypes = {
    title : PropTypes.string.isRequired, 
    img: PropTypes.string.isRequired, 
    id: PropTypes.string.isRequired, 
    price: PropTypes.number.isRequired, 
    color: PropTypes.string, 
    isFavorite: PropTypes.bool, 
    addToCart: PropTypes.func, 
    addToFavorites: PropTypes.func, 
    setIsOpenModal: PropTypes.func, 
    setModalProps: PropTypes.func
}

CardItem.defaultProps = {
    color: "", 
    isFavorite: false, 
    addToCart: () => {}, 
    addToFavorites: () => {}, 
    setIsOpenModal: () => {}, 
    setModalProps: () => {}
}

export default CardItem