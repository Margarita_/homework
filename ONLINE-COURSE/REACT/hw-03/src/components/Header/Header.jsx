import style from "./Header.module.scss";
import PropTypes from "prop-types";
import Navigation from "../Navigation/Navigation";

const Header = ({ cartCount, favoriteCount }) => {
    return (
        <div className={style.headerSection}>
           <Navigation cartCount={cartCount} favoriteCount={favoriteCount}/>
        </div>
    )
}

Header.propTypes = {
    cartCount: PropTypes.number,
    favoriteCount: PropTypes.number
}

Header.defaultProps = {
    cartCount: 0,
    favoriteCount: 0
}

export default Header