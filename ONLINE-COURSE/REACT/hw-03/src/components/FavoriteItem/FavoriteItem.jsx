import PropTypes from "prop-types";
import style from "./FavoriteItem.module.scss"
import Button from "../Button/Button";

const FavoriteItem = ({ title, img, id, price, color, setFavorites, setIsOpenModal, setModalProps, addToCart, isFavorite}) => {
        return (
            <div className={style.cardWrap}>
                <img className={style.productImg} src={img} alt={title} />

                <button className={style.deleteButton} 
                        onClick={()=>{
                                setIsOpenModal(true);
                                setModalProps({
                                    header: "Remove from favorites?", 
                                    text: `Are you sure you want remove ${title} from favorites list?`, 
                                    onSubmit: ()=>{
                                        setFavorites((current) => {
                                            const newFav = [...current];
                                            newFav.forEach((el, index) => {
                                                if (el === id) {
                                                    newFav.splice(index, 1)
                                                }
                                            })
                                            localStorage.setItem("favorites", JSON.stringify(newFav))
                                            return newFav
                                        })
                                        
                                        setIsOpenModal(false);
                                    }
                                })
                }}>
                </button>

                <p className={style.title}>{title}</p>
                <p className={style.price}>{price} &#8372;</p>
                <div className={style.color}>
                    <p>Color:</p>
                    <div style={{backgroundColor: color}}></div>
                </div>
                <p className={style.id}>Code: {id}</p>


                <Button 
                text="Add to cart" 
                backgroundColor="#123456" 
                onClick={() => {
                    setModalProps({
                        header: "Add to cart", 
                        text: `Are you sure you want add to cart ${title}?`, 
                        onSubmit: ()=>{
                            addToCart({title, img, id, price, color, isFavorite})
                            setIsOpenModal(false);
                        }
                    });
                    setIsOpenModal(true);
                }}/>

            </div>
        )
    }

    FavoriteItem.propTypes = {
    title : PropTypes.string.isRequired, 
    img: PropTypes.string.isRequired, 
    id: PropTypes.string.isRequired, 
    price: PropTypes.number.isRequired, 
    color: PropTypes.string, 
    isFavorite: PropTypes.bool, 
    addToCart: PropTypes.func, 
    setFavorites: PropTypes.func, 
    setIsOpenModal: PropTypes.func, 
    setModalProps: PropTypes.func
}

FavoriteItem.defaultProps = {
    color: "", 
    isFavorite: false, 
    addToCart: () => {}, 
    setFavorites: () => {}, 
    setIsOpenModal: () => {}, 
    setModalProps: () => {}
}

export default FavoriteItem