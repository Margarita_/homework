import PropTypes from "prop-types";
import style from "./CartItem.module.scss"

const CartItem = ({ title, img, id, price, color, setCart, setIsOpenModal, setModalProps}) => {
        return (
            <div className={style.cardWrap}>
                <img className={style.productImg} src={img} alt={title} />

                <button className={style.deleteButton} 
                        onClick={()=>{
                                setIsOpenModal(true);
                                setModalProps({
                                    header: "Delete item", 
                                    text: `Are you sure you want delete from cart ${title}?`, 
                                    onSubmit: ()=>{
                                        setCart((current) => {
                                            const newCart = [...current];
                                            newCart.forEach((el, index) => {
                                                if (el.id === id) {
                                                    newCart.splice(index, 1)
                                                }
                                            })
                                            localStorage.setItem("cart", JSON.stringify(newCart))
                                            return newCart
                                        })
                                        
                                        setIsOpenModal(false);
                                    }
                                })
                }}>
                </button>

                <p className={style.title}>{title}</p>
                <p className={style.price}>{price} &#8372;</p>
                <div className={style.color}>
                    <p>Color:</p>
                    <div style={{backgroundColor: color}}></div>
                </div>
                <p className={style.id}>Code: {id}</p>

            </div>
        )
    }

CartItem.propTypes = {
    title : PropTypes.string.isRequired, 
    img: PropTypes.string.isRequired, 
    id: PropTypes.string.isRequired, 
    price: PropTypes.number.isRequired, 
    color: PropTypes.string, 
    isFavorite: PropTypes.bool,
    setIsOpenModal: PropTypes.func, 
    setModalProps: PropTypes.func
}

CartItem.defaultProps = {
    color: "", 
    isFavorite: false,
    setIsOpenModal: () => {}, 
    setModalProps: () => {}
}

export default CartItem