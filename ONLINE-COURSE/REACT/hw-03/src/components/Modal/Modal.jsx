import style from "./Modal.module.scss"
import PropTypes from "prop-types"
import Button from "../Button/Button";

const Modal = ({ modalProps:{header, text, onSubmit}, setIsOpenModal }) => {
        

        return ( <div className={style.backgroundContainer} onClick = {()=>{setIsOpenModal(false)}}>
                <div className={style.contentContainer} onClick = {event => event.stopPropagation()}>
                    <div className={style.header}>
                        <p>{header}</p>
                        <button className={style.closeButton} onClick = {()=>{setIsOpenModal(false)}}></button>
                    </div>
                    <div className={style.body}>
                        <p>{text}</p>
                    </div>
                    <div className={style.footer}>
                        <Button 
                        text="Submit" 
                        backgroundColor="#123456" 
                        onClick={onSubmit}/>
                        <Button 
                        text="Cancel" 
                        backgroundColor="grey" 
                        onClick={()=>{setIsOpenModal(false)}}/>
                    </div>
                </div>
            </div>)
    }

Modal.propTypes = {
    modalPrors: PropTypes.shape({
        header: PropTypes.string,
        text: PropTypes.string,
        onSubmit: PropTypes.func
    }),
    setIsOpenModal: PropTypes.func
}

Modal.defaultProps = {
    modalPrors : {
        header: "Modal title",
        text: "Modal text",
        onSubmit: () => {console.log("onSubmit");}
    },
    setIsOpenModal: () => {}
}

export default Modal