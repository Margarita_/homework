import { ADD_TO_CART, DELETE_FROM_CART, GET_CART_FROM_LS } from "./actions";


export const getCartFromLsAC = () => {
    if (localStorage.getItem("cart")){
        const cart = JSON.parse(localStorage.getItem("cart"))
        return {type: GET_CART_FROM_LS, payload: cart}
       } else {
        return {type: GET_CART_FROM_LS, payload: []}
       }
    }


export const addToCartAC = (product) => ({type: ADD_TO_CART, payload: product})

export const deleteFromCartAC = (id) => ({type: DELETE_FROM_CART, payload: id})