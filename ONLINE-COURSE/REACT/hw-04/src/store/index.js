import {createStore} from "redux"
import { composeWithDevTools } from "redux-devtools-extension";
import { applyMiddleware } from "redux"
import thunk from "redux-thunk";
import appReducer from "./appReducer"

const store = createStore(appReducer, composeWithDevTools(applyMiddleware(thunk)))

export default store