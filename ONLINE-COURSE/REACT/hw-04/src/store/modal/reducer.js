import { SET_IS_OPEN_MODAL, SET_MODAL_DATA } from "./actions"
import produce from "immer"

const initialState = {
    isOpenModal: false,
    modalData: {
        header: "",
        text: "",
        onSubmit: () => {}
    }
}

const modalReducer = (state = initialState, {type, payload}) => {
    switch (type) {
         case SET_IS_OPEN_MODAL: {
            return produce(state, draftState => {draftState.isOpenModal = payload})
         }

         case SET_MODAL_DATA: {
            return produce(state, draftState => {draftState.modalData = payload})
         }
        default: {return state}
    }
}

export default modalReducer