import { GET_PRODUCTS_FROM_SERVER, SET_IS_FAVORITE, TOGGLE_IS_FAVORITE, DECR_FAVORITE_COUNT, INCR_FAVORITE_COUNT } from "./actions";

export const getProductsAC = () => async (dispatch) => {
    let products = []
    if (localStorage.getItem("products")) {
        products = JSON.parse(localStorage.getItem("products"));
    } else {
        try {
            products = await fetch("./products.json").then(res => res.json());
            localStorage.setItem("products", JSON.stringify(products))
        } catch (error) {
            console.warn(`CATCH: ${error}`);
        }
    }
    products.forEach(el => {
        if(el.isFavorite){dispatch({type: INCR_FAVORITE_COUNT})}
    })
    dispatch({ type: GET_PRODUCTS_FROM_SERVER, payload: products })
}


export const toggleIsFavoriteAC = (id) => (dispatch) => {
    //here should be a request to server
    const productsLS = JSON.parse(localStorage.getItem("products"))
    productsLS.forEach(el => {
        if (el.id === id) {
            el.isFavorite = !el.isFavorite
            el.isFavorite ? dispatch({type: INCR_FAVORITE_COUNT}) : dispatch({type: DECR_FAVORITE_COUNT})
        }
    })
    localStorage.setItem("products", JSON.stringify(productsLS))
    dispatch({ type: TOGGLE_IS_FAVORITE, payload: id })
}


export const setIsFavoriteAC = ({id, value}) => (dispatch) => {
        //here should be a request to server
        const productsLS = JSON.parse(localStorage.getItem("products"))
        productsLS.forEach(el => {
            if(el.id === id) {
                el.isFavorite = value
                el.isFavorite ? dispatch({type: INCR_FAVORITE_COUNT}) : dispatch({type: DECR_FAVORITE_COUNT})
            }
        })
        localStorage.setItem("products", JSON.stringify(productsLS))
        dispatch({type: SET_IS_FAVORITE, payload: {id, value}})
    };