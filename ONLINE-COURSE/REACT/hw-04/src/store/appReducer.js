import { combineReducers } from "redux";
import cartReducer from "./cart/reducer";
import modalReducer from "./modal/reducer";
import productsReducer from "./products/reducer";

const appReducer = combineReducers({
    products: productsReducer,
    cart: cartReducer,
    modal: modalReducer
})

export default appReducer