import FavoriteItem from "../../components/FavoriteItem/FavoriteItem"
import style from "./FavoritesPage.module.scss"
import { useSelector } from "react-redux";

const FavoritesPage = () => {

    const products = useSelector(store => store.products.data)

    return (
        <div className={style.container}>
            {products.map(product => {
                if (product.isFavorite) {

                    return (<FavoriteItem
                        key={product.id}
                        title={product.title}
                        img={product.img}
                        id={product.id}
                        price={product.price}
                        color={product.color}
                        isFavorite={product.isFavorite}
                    />)
                }
            })}

        </div>
    )
}


export default FavoritesPage