import CartItem from "../../components/CartItem/CartItem"
import style from "./CartPage.module.scss"
import { useSelector } from "react-redux"

const CartPage = () => {
    const cart = useSelector(store => store.cart.data)
    return (
    <section className={style.container}>
            {cart.map(el => {
            return (
                <CartItem key={el.id}
                title={el.title}
                img={el.img}
                id={el.id}
                price={el.price}
                color={el.color}
                count={el.count}
                />)
            })}
     </section>
    )
}


export default CartPage