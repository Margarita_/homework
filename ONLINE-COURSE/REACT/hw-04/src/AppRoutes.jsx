import { Route, Routes } from "react-router-dom"
import CartPage from "./pages/CartPage/CartPage"
import FavoritesPage from "./pages/FavoritesPage/FavoritesPage"
import HomePage from "./pages/HomePage/HomePage"

const AppRoutes = () => {
    return (
        <Routes>
            <Route path="/" element={ <HomePage/> } />
            <Route path="/cart" element={<CartPage/>} />
            <Route path="/favorites" element={<FavoritesPage/>} />
        </Routes>
    )

}

export default AppRoutes