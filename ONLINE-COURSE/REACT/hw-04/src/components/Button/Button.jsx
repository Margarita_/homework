import React from "react";
import style from "./Button.module.scss";
import PropTypes from "prop-types";

const Button = ({backgroundColor, text, onClick, type}) => {

        return (
            <button className={style.button} style={{backgroundColor}} onClick={onClick} type={type}> {text}</button>
        )
}

Button.propTypes = {
    onClick : PropTypes.func.isRequired,
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    type: PropTypes.oneOf(["button", "submit", "reset"]),
}

Button.defaultProps = {
    backgroundColor: "#2632a0",
    text: "Button",
    type: "button",
}

export default Button