import style from "./Modal.module.scss"
import PropTypes from "prop-types"
import Button from "../Button/Button";
import { useDispatch, useSelector } from "react-redux";
import { setIsOpenModalAC } from "../../store/modal/actionCreators";

const Modal = () => {
    
    const {text, title, onSubmit} = useSelector(store => store.modal.modalData)
    const isOpen = useSelector(store => store.modal.isOpenModal)
    const dispatch = useDispatch()
        if(isOpen) {
            return ( <div className={style.backgroundContainer} onClick = {()=>{dispatch(setIsOpenModalAC(false))}}>
                <div className={style.contentContainer} onClick = {event => event.stopPropagation()}>
                    <div className={style.header}>
                        <p>{title}</p>
                        <button className={style.closeButton}
                        onClick = {()=>{dispatch(setIsOpenModalAC(false))}}>
                        </button>
                    </div>
                    <div className={style.body}>
                        <p>{text}</p>
                    </div>
                    <div className={style.footer}>
                        <Button 
                        text="Submit" 
                        backgroundColor="#123456" 
                        onClick={onSubmit}/>
                        <Button 
                        text="Cancel" 
                        backgroundColor="grey" 
                        onClick={()=>{dispatch(setIsOpenModalAC(false))}}/>
                    </div>
                </div>
            </div>)
        }  
    }

Modal.propTypes = {
    modalPrors: PropTypes.shape({
        header: PropTypes.string,
        text: PropTypes.string,
        onSubmit: PropTypes.func
    }),
    setIsOpenModal: PropTypes.func
}

Modal.defaultProps = {
    modalPrors : {
        header: "Modal title",
        text: "Modal text",
        onSubmit: () => {console.log("onSubmit");}
    },
    setIsOpenModal: () => {}
}

export default Modal