import { Component } from 'react';
import './App.module.scss';
import Header from "./components/Header/Header"
import CardList from './components/CardList/CardList';
import Modal from "./components/Modal/Modal"

class App extends Component {

  state = {
    products: [],
    cart: [],
    favorites: [],
    isOpenModal: false,
    modalProps: {},
    test: 23
  }

  addToCart = (card) => {
    this.setState((current) => {
      const newCart = [...current.cart, card]
      localStorage.setItem("cart", JSON.stringify(newCart))
      return {cart:newCart}
    })
  }

  addToFavorites = (id) => {
    this.setState((current) => {
      const favoritesId = [...current.favorites, id]
      localStorage.setItem("favorites", JSON.stringify(favoritesId))

      const products = [...current.products]
      const index = products.findIndex(el => el.id === id)
      products[index].isFavorite = true
      return {favorites: favoritesId, products}
  })
  }

  setIsOpenModal = (value) => {
    this.setState({ isOpenModal: value })
  }

  setModalProps = (obj) => {
    this.setState({modalProps : obj})
    console.log(this.state.modalProps);

  }

  async componentDidMount() {
    try {
      const products = await fetch("./products.json").then(res => res.json())
      this.setState({products})
    } catch (error) {
      console.warn(`CATCH: ${error}`);
    }

    const cart = localStorage.getItem("cart")
    if (cart) {this.setState({cart : JSON.parse(cart)})}

    const favoritesStorage = localStorage.getItem("favorites") 
    if (favoritesStorage) {
      const favorites = JSON.parse(favoritesStorage)
      this.setState((current) => {
        const products = [...current.products]
        products.forEach(product => {
          if (favorites.indexOf(product.id) >= 0) {
            product.isFavorite = true
          }
        })

        return {products, favorites}
      })
    }
  }

  render() {
    const {products, cart, favorites, isOpenModal, modalProps} = this.state
    return (
      <div className="App">
        <Header cartCount={cart.length} favoriteCount={favorites.length} />
        <CardList 
        cardArray = {products} 
        addToCart={this.addToCart} 
        addToFavorites={this.addToFavorites}
        setIsOpenModal={this.setIsOpenModal}
        setModalProps={this.setModalProps}/>

        {isOpenModal && <Modal
        modalProps={modalProps}
        setIsOpenModal={this.setIsOpenModal} />}
      </div>
    );
  }

}

export default App;
