import { PureComponent } from "react";
import style from "./Header.module.scss";
import favoriteIcon from "./star-icon.svg"
import cartIcon from "./cart-icon.svg"
import PropTypes from "prop-types"

class Header extends PureComponent {
    render() {
        const { cartCount, favoriteCount } = this.props
        return (
            <div className={style.headerSection}>
                <div className={style.container}>
                    <div className={style.logoWrap}>
                        <a href="#">Logo</a>
                    </div>

                    <div className={style.iconWrap}>
                        <div className={style.icon}>
                            <img src={favoriteIcon} alt="" />
                            {(favoriteCount > 0) && <p>{favoriteCount}</p>}
                        </div>

                        <div className={style.icon}>
                            <img src={cartIcon} alt="" />
                            { (cartCount > 0) && <p>{cartCount}</p>}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

Header.propTypes = {
    cartCount: PropTypes.number,
    favoriteCount: PropTypes.number
}

Header.defaultProps = {
    cartCount: 0,
    favoriteCount: 0
}

export default Header