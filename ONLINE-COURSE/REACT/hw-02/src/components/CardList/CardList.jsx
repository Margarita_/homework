import { PureComponent } from "react";
import PropTypes from "prop-types";
import style from "./CardList.module.scss"
import CardItem from "../CardItem/CardItem"; 

class CardList extends PureComponent {
    render() {
        const {cardArray, addToCart, addToFavorites, setIsOpenModal, setModalProps} = this.props
        return(
            <div className={style.container}>
                {cardArray.map(el => <CardItem key={el.id}
                    id={el.id}
                    title={el.title}
                    color={el.color}
                    price={el.price}
                    isFavorite={el.isFavorite}
                    img={el.img}
                    addToCart={addToCart}
                    addToFavorites={addToFavorites}
                    setModalProps={setModalProps}
                    setIsOpenModal={setIsOpenModal}/>)}
            </div>
        )
    }
}

CardList.propTypes = {
    cardArray: PropTypes.arrayOf(PropTypes.object),
    addToCart: PropTypes.func,
    addToFavorites: PropTypes.func,
    setIsOpenModal: PropTypes.func,
    setModalProps: PropTypes.func
}

CardList.defaultProps = {
    cardArray: [],
    addToCart: ()=>{},
    addToFavorites: () => {},
    setIsOpenModal: () => {},
    setModalProps: () => {}
}

export default CardList