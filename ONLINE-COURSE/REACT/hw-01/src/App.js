import style from './App.module.scss';
import React from 'react';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';

class App extends React.Component {

  state = {
      isOpenModal : false,
      modalProps: {}
}


closeModal = () => {
  this.setState({isOpenModal:false})
}

showFirstModal = () => {
  this.setState({isOpenModal:true})
  
  this.setState({modalProps:{
    header: "First modal",
    addCloseButton: true,
    text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
    onClose: this.closeModal,
    action: <div className={style.buttonWrap}>
      <Button text = "Submit" backgroundColor="#2632a0" onClick={() => {
          console.log("Submit from first modal"); this.closeModal()
          } }/>
    </div>
  }})
}

showSecondModal = () => {
  this.setState({isOpenModal:true})
  
  this.setState({modalProps:{
    header: "Second modal",
    addCloseButton: false,
    text: "Amet consectetur adipisicing elit. Vitae doloremque culpa sint.",
    onClose: this.closeModal,
    action: <div className={style.buttonWrap}>
    <Button text = "OK" backgroundColor="#ffee00" onClick={() => {
        console.log("OK from second modal"); this.closeModal()
        } }/>
    <Button text = "Cancel" backgroundColor="grey" onClick={this.closeModal}/>
  </div>
  }})
}



  render(){
  const {isOpenModal, modalProps} = this.state
  return (
      <main className={style.buttonSection}>
        <section className={style.sectionContent}>
          <Button text = "Open first modal" backgroundColor="#2632a0" onClick={this.showFirstModal}/>
          <Button text = "Open second modal" backgroundColor="#ffee00" onClick={this.showSecondModal}/>
        </section>
      {isOpenModal && <Modal propsObj = {modalProps}/>}
      </main>
  );
}
}

export default App;
