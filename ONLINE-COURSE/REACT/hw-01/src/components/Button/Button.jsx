import React from "react";
import style from "./Button.module.scss"

class Button extends React.PureComponent {
    render(){
        const {backgroundColor, text, onClick} = this.props

        return (
            <button className={style.button} style={{backgroundColor}} type="button" onClick={onClick}>{text}</button>
        )
    }
}

export default Button