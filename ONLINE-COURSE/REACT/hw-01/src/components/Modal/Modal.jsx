import React from "react";
import style from "./Modal.module.scss"

class Modal extends React.PureComponent {


    render() {
        const { propsObj } = this.props
        const { addCloseButton, header, text, action, onClose } = propsObj

        return ( <div className={style.backgroundContainer} onClick = {onClose}>
                <div className={style.contentContainer} onClick = {event => event.stopPropagation()}>
                    <div className={style.header}>
                        <p>{header}</p>
                        {addCloseButton && <button className={style.closeButton} onClick = {onClose}></button>}
                    </div>
                    <div className={style.body}>
                        <p>{text}</p>
                    </div>
                    <div className={style.footer}>
                    {action}
                    </div>
                </div>
            </div>)
    }
}

export default Modal