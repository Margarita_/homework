import style from "./CartPage.module.scss"
import { useSelector } from "react-redux"
import BuyForm from "../../components/BuyForm/BuyForm"
import CartItem from "../../components/CartItem/CartItem"

const CartPage = () => {
    const cart = useSelector(store => store.cart.data)
    return (
        <div className={style.sectionContainer}>
            <section>
                <div className={style.buyForm}>
                    <BuyForm />
                </div>
            </section>

            {cart.length === 0 ? <h3 className={style.noItemsMessage}>You have no items in the cart</h3> :
                <section className={style.cardContainer}>
                    {cart.map(el => {
                        return (
                            <CartItem key={el.id}
                                title={el.title}
                                img={el.img}
                                id={el.id}
                                price={el.price}
                                color={el.color}
                                count={el.count}
                            />)
                    })}
                </section>
            }
        </div>
    )
}


export default CartPage