import { ADD_TO_CART, CLEAN_CART, DELETE_FROM_CART, GET_CART_FROM_LS } from "./actions"
import produce from "immer"

const initialState = {
    data: []
}

const cartReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case GET_CART_FROM_LS : {
            return produce(state, draftState => {draftState.data = payload})
        }

        case ADD_TO_CART: {
            return produce(state, draftState => {
                const index = draftState.data.findIndex(el => el.id === payload.id)
                if (index > -1) {draftState.data[index].count += 1} else {
                    draftState.data.push({...payload, count : 1})
                }
                localStorage.setItem("cart", JSON.stringify(draftState.data))
            })
        }

        case DELETE_FROM_CART: {
            return produce(state, draftState => {
                const index = draftState.data.findIndex(el => el.id === payload)
                draftState.data.splice(index, 1)
                localStorage.setItem("cart", JSON.stringify(draftState.data))
            })
        }

        case CLEAN_CART: {
            return produce(state, draftState => {draftState.data = []})
        }

        default: {return state}
    }
}

export default cartReducer