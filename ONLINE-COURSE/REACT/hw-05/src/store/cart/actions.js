export const ADD_TO_CART  = "ADD_TO_CART"
export const DELETE_FROM_CART  = "DELETE_FROM_CART"
export const GET_CART_FROM_LS  = "GET_CART_FROM_LS"
export const CLEAN_CART = "CLEAN_CART"