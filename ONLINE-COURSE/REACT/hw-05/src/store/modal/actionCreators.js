import { SET_IS_OPEN_MODAL, SET_MODAL_DATA } from "./actions";

export const setIsOpenModalAC = (value) => ({type:SET_IS_OPEN_MODAL, payload: value})
export const setModalDataAC = (modalDataObj) => ({type: SET_MODAL_DATA, payload: modalDataObj})