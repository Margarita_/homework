import { DECR_FAVORITE_COUNT, GET_PRODUCTS_FROM_SERVER, INCR_FAVORITE_COUNT, SET_IS_FAVORITE, TOGGLE_IS_FAVORITE } from "./actions"
import produce from "immer"

const initialState = {
    data: [],
    favoriteCount: 0
}

const productsReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case GET_PRODUCTS_FROM_SERVER : {
            return produce(state, draftState => {
                draftState.data = payload
            })
        }

        case SET_IS_FAVORITE : {
            return produce(state, draftState => {
                draftState.data.forEach(el => {
                    if(el.id === payload.id) {
                        el.isFavorite = payload.value
                    }
                })
            })
        }

        case TOGGLE_IS_FAVORITE: {
            return produce(state, draftState => {
                draftState.data.forEach(el => {
                    if(el.id === payload) {
                        el.isFavorite = !el.isFavorite
                    }
                })
            })
        }

        case INCR_FAVORITE_COUNT: {
            return produce(state, draftState => {draftState.favoriteCount += 1})
        }

        case DECR_FAVORITE_COUNT: {
            return produce(state, draftState => {draftState.favoriteCount -= 1})
        }

        default: {return state}
    }
}

export default productsReducer