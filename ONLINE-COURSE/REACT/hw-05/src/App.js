import { useEffect, useState } from 'react';
import './App.module.scss';
import Header from "./components/Header/Header"
import Modal from "./components/Modal/Modal"
import AppRoutes from "./AppRoutes"
import { useDispatch, useSelector } from 'react-redux';
import { getProductsAC } from './store/products/actionCreators';
import { getCartFromLsAC } from './store/cart/actionCreators';

const App = () => {

  const dispatch = useDispatch()
  const productsStore = useSelector(store => store.products.data)

  const [cart, setCart] = useState([]);
  const [favorites, setFavorites] = useState([]);
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [modalProps, setModalProps] = useState({});

  const addToCart = (card) => {
    setCart((current) => {
        const newCart = [...current, card]
        localStorage.setItem("cart", JSON.stringify(newCart))
        return newCart
    })

}

  useEffect(() => {
    dispatch(getProductsAC())
    dispatch(getCartFromLsAC())
  }, [])

  return (
    <div className="App">
      <Header cartCount={cart.length} favoriteCount={favorites.length}/>
      <AppRoutes/>
      <Modal/>
    </div>
  )
}

export default App;
