import PropTypes from "prop-types";
import style from "./CardList.module.scss"
import CardItem from "../CardItem/CardItem"; 
import { useSelector } from "react-redux";

const CardList = ({ addToCart, addToFavorites, setIsOpenModal, setModalProps}) => {

    const cardArray = useSelector(store => store.products.data)

        return(
            <div className={style.container}>
                {cardArray.map(el => <CardItem key={el.id}
                    id={el.id}
                    title={el.title}
                    color={el.color}
                    price={el.price}
                    isFavorite={el.isFavorite}
                    img={el.img}/>)}
            </div>
        )
}

CardList.propTypes = {
    cardArray: PropTypes.arrayOf(PropTypes.object),
}

CardList.defaultProps = {
    cardArray: [],
}

export default CardList