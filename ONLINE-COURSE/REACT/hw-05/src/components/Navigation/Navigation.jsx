import style from "./Navigation.module.scss";
import favoriteIcon from "./star-icon.svg"
import cartIcon from "./cart-icon.svg"
import PropTypes from "prop-types"
import { Link } from "react-router-dom";

const Navigation = ({cartCount, favoriteCount}) => {
    
    return (
            <div className={style.container}>
                <Link to="/" className={style.logoWrap}>
                    Home
                </Link>

                <div className={style.iconWrap}>
                    <Link to="/favorites" className={style.icon}>
                        <img src={favoriteIcon} alt="" />
                        {(favoriteCount > 0) && <p>{favoriteCount}</p>}
                    </Link>


                    <Link to="/cart" className={style.icon}>
                        <img src={cartIcon} alt="" />
                        {(cartCount > 0) && <p>{cartCount}</p>}
                    </Link>
                </div>
            </div>
    )
}

Navigation.propTypes = {
    cartCount: PropTypes.number,
    favoriteCount: PropTypes.number
}

Navigation.defaultProps = {
    cartCount: 0,
    favoriteCount: 0
}

export default Navigation