export const getTotalprice = (cart) => {
    let total = 0;
    cart.forEach(el => {
        total = total + (el.price * el.count)
    })
    return total
}

export const getProductList = (cart) => {
    let list = ""
    cart.forEach((el, index) => {
        if (index === 0) {
            list = list + `${el.title}`
        } else {
            list = list + `, ${el.title}`
        }
    })
    return list;
}