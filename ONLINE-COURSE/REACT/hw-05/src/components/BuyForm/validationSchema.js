import * as yup from "yup"

const validationSchema = yup.object().shape({
    name: yup.string()
        .min(3, "Min 3 symbols")
        .max(20, "Max 20 symbols")
        .matches(/[A-Za-z\s]/g, "Should contain only characters or space")
        .required("Name is a required field"),

    lastName: yup.string()
        .min(3, "Min 3 symbols")
        .max(20, "Max 20 symbols")
        .matches(/[A-Za-z\s]/g, "Should contain only characters or space")
        .required("Last name is a required field"),

    adress: yup.string().required("Adress is a required field"),

    age: yup.number()
        .min(8, "Age should be more then 8")
        .max(110, "Age should be less then 110")
        .required("Age is a required field"),

    tel: yup.string("Tel is a required field")
        .matches(/^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/g, "Invalid phone number")
})

export default validationSchema