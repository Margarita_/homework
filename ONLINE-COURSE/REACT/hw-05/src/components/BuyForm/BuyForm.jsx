import { Formik, Form } from "formik";
import Input from "../Input/Input";
import style from "./BuyForm.module.scss"
import Button from "../Button/Button"
import { useDispatch, useSelector } from "react-redux";
import { cleanCartAC } from "../../store/cart/actionCreators";
import { setIsOpenModalAC, setModalDataAC } from "../../store/modal/actionCreators";
import validationSchema from "./validationSchema";
import { getTotalprice, getProductList } from "./utils";


const BuyForm = () => {

    const dispatch = useDispatch()
    const cart = useSelector(store => store.cart.data);

    const initialValues = {
        name: "",
        lastName: "",
        age: "",
        adress: "",
        tel: "+380"
    }

    const onSubmit = (values, handlers) => {
        dispatch(setIsOpenModalAC(true))
        dispatch(setModalDataAC({
            title: "Buy this amazing things?",
            text: `You will buy ${getProductList(cart)}. Total price is: ${getTotalprice(cart)} UAH.`,
            onSubmit: () => {
                console.log(cart);
                console.log(values);
                dispatch(cleanCartAC());
                handlers.resetForm()
                dispatch(setIsOpenModalAC(false))
            }
        }))
    }

    const isEmptyCart = (cart.length === 0)

    return (
        <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
            {({isValid, dirty}) => {
                return (
                    <Form>
                        
                        <div className={style.formWrap}>
                            <h3 className={style.title}>Сomplete the form to complete your purchase:</h3>
                            <Input disabled={isEmptyCart} name="name" type="text" label="Name:" />
                            <Input disabled={isEmptyCart} name="lastName" type="text" label="Last name:" />
                            <Input disabled={isEmptyCart} name="age" type="text" label="Age:" />
                            <Input disabled={isEmptyCart} name="adress" type="text" label="Adress:" />
                            <Input disabled={isEmptyCart} name="tel" type="text" label="Tel:" />

                            <Button disabled={!dirty || !isValid || isEmptyCart} type="submit" text="Buy" />
                        </div>
                    </Form>
                )
            }}
        </Formik>
    )
}

export default BuyForm