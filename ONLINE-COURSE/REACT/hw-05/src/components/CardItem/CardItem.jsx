import PropTypes from "prop-types";
import style from "./CardItem.module.scss"
import starFilled from "./star-filled.svg"
import starOutline from "./star-outline.svg"
import Button from "../Button/Button"
import { useDispatch } from "react-redux";
import { toggleIsFavoriteAC } from "../../store/products/actionCreators";
import { addToCartAC } from "../../store/cart/actionCreators";
import { setIsOpenModalAC, setModalDataAC } from "../../store/modal/actionCreators";

const CardItem = ({ title, img, id, price, color, isFavorite }) => {

    const dispatch = useDispatch()

        return (
            <div className={style.cardWrap}>
                <img className={style.productImg} src={img} alt={title} />

                <button className={style.favoriteButton}
                        onClick={() => {dispatch(toggleIsFavoriteAC(id))}}>
                    <img className={style.favoriteIcon} src={isFavorite ? starFilled : starOutline} alt="" />
                </button>

                <p className={style.title}>{title}</p>
                <p className={style.price}>{price} &#8372;</p>
                <div className={style.color}>
                    <p>Color:</p>
                    <div style={{backgroundColor: color}}></div>
                </div>
                <p className={style.id}>Code: {id}</p>

                <Button 
                text="Add to cart" 
                backgroundColor="#123456" 
                onClick={() => {
                    dispatch(setIsOpenModalAC(true))
                    dispatch(setModalDataAC({
                        title: `Add to cart`,
                        text: `You want to add to cart ${title}?`,
                        onSubmit: () => {
                            dispatch(addToCartAC({title, img, id, price, color, isFavorite}))
                            dispatch(setIsOpenModalAC(false))
                        }
                    }))
                    
                    
                    }}/>
            </div>
        )
    }

CardItem.propTypes = {
    title : PropTypes.string.isRequired, 
    img: PropTypes.string.isRequired, 
    id: PropTypes.string.isRequired, 
    price: PropTypes.number.isRequired, 
    color: PropTypes.string, 
    isFavorite: PropTypes.bool,
}

CardItem.defaultProps = {
    color: "", 
    isFavorite: false,
}

export default CardItem