import { useField } from "formik"
import style from "./Input.module.scss"

const Input = (props) => {
    const [field, meta] = useField(props)
    const {type, label, disabled} = props

    return (
        <div className={style.inputWrap}>
        <label className={style.label}>{label}</label>

        <input disabled={disabled} className={style.input}
        type={type}
        {...field}
        />

        {meta.error && meta.touched && <span className={style.errorMessage}>{meta.error}</span>}
        </div>
    )
}

export default Input