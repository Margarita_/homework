import PropTypes from "prop-types";
import style from "./FavoriteItem.module.scss"
import Button from "../Button/Button";
import { useDispatch } from "react-redux";
import { addToCartAC } from "../../store/cart/actionCreators";
import { setIsFavoriteAC } from "../../store/products/actionCreators";
import { setIsOpenModalAC, setModalDataAC } from "../../store/modal/actionCreators";

const FavoriteItem = ({ title, img, id, price, color, isFavorite}) => {
    const dispatch = useDispatch()
        return (
            <div className={style.cardWrap}>
                <img className={style.productImg} src={img} alt={title} />

                <button className={style.deleteButton} 
                        onClick={()=>{
                            dispatch(setIsOpenModalAC(true))
                            dispatch(setModalDataAC({
                                title: `Delete from favorite?`,
                                text: `Are you sure you want to delete ${title} from favorite?`,
                                onSubmit: () => {
                                    dispatch(setIsFavoriteAC({id, value: false}));
                                    dispatch(setIsOpenModalAC(false))
                                }
                            }))
                            
                            }}>
                </button>

                <p className={style.title}>{title}</p>
                <p className={style.price}>{price} &#8372;</p>
                <div className={style.color}>
                    <p>Color:</p>
                    <div style={{backgroundColor: color}}></div>
                </div>
                <p className={style.id}>Code: {id}</p>


                <Button 
                text="Add to cart" 
                backgroundColor="#123456" 
                onClick={() => {
                    dispatch(setIsOpenModalAC(true))
                    dispatch(setModalDataAC({
                        title: `Add to cart`,
                        text: `You want to add to cart ${title}?`,
                        onSubmit: () => {
                            dispatch(addToCartAC({title, img, id, price, color, isFavorite}))
                            dispatch(setIsOpenModalAC(false))
                        }
                    }))}}/>

            </div>
        )
    }

    FavoriteItem.propTypes = {
    title : PropTypes.string.isRequired, 
    img: PropTypes.string.isRequired, 
    id: PropTypes.string.isRequired, 
    price: PropTypes.number.isRequired, 
    color: PropTypes.string, 
    isFavorite: PropTypes.bool, 
    addToCart: PropTypes.func, 
    setFavorites: PropTypes.func, 
    setIsOpenModal: PropTypes.func, 
    setModalProps: PropTypes.func
}

FavoriteItem.defaultProps = {
    color: "", 
    isFavorite: false, 
    addToCart: () => {}, 
    setFavorites: () => {}, 
    setIsOpenModal: () => {}, 
    setModalProps: () => {}
}

export default FavoriteItem