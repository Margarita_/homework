import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import { deleteFromCartAC } from "../../store/cart/actionCreators";
import { setIsOpenModalAC, setModalDataAC } from "../../store/modal/actionCreators";
import style from "./CartItem.module.scss"

const CartItem = ({ title, img, id, price, color, count}) => {

    
    const dispatch = useDispatch()
        return (
            <div className={style.cardWrap}>
                <img className={style.productImg} src={img} alt={title} />

                <button className={style.deleteButton} 
                        onClick={ () => {
                            dispatch(setIsOpenModalAC(true));
                            dispatch(setModalDataAC({
                                title: `Delete product`,
                                text: `Are you sure you want to delete ${title} from cart?`,
                                onSubmit: () => {
                                    dispatch(deleteFromCartAC(id))
                                    dispatch(setIsOpenModalAC(false))
                                }
                            }))
                            }}>
                </button>

                <p className={style.title}>{title}</p>
                <p className={style.price}>{price} &#8372;</p>
                <div className={style.color}>
                    <p>Color:</p>
                    <div style={{backgroundColor: color}}></div>
                </div>
                <p>Count: {count}</p>
                <p className={style.id}>Code: {id}</p>

            </div>
        )
    }

CartItem.propTypes = {
    title : PropTypes.string.isRequired, 
    img: PropTypes.string.isRequired, 
    id: PropTypes.string.isRequired, 
    price: PropTypes.number.isRequired, 
    color: PropTypes.string, 
    isFavorite: PropTypes.bool,
    count: PropTypes.number
}

CartItem.defaultProps = {
    color: "", 
    isFavorite: false,
}

export default CartItem