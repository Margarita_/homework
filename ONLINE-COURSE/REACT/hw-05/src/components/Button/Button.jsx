import React from "react";
import style from "./Button.module.scss";
import PropTypes from "prop-types";

const Button = ({backgroundColor, text, onClick, type, disabled}) => {

        return (
            <button disabled={disabled} className={style.button} style={{backgroundColor}} onClick={onClick} type={type}> {text}</button>
        )
}

Button.propTypes = {
    onClick : PropTypes.func,
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    type: PropTypes.oneOf(["button", "submit", "reset"]),
}

Button.defaultProps = {
    onClick: () => {},
    disabled: false,
    backgroundColor: "#2632a0",
    text: "Button",
    type: "button",
}

export default Button