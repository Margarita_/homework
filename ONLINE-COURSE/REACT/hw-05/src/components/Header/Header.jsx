import style from "./Header.module.scss";
import PropTypes from "prop-types";
import Navigation from "../Navigation/Navigation";
import { useSelector } from "react-redux";

const Header = () => {

    const favoriteCount = useSelector(store => store.products.favoriteCount)
    const cart = useSelector(store => store.cart.data)
    let cartCount = 0;
    cart.forEach(el => cartCount = cartCount + el.count)

    return (
        <div className={style.headerSection}>
           <Navigation cartCount={cartCount} favoriteCount={favoriteCount}/>
        </div>
    )
}

Header.propTypes = {
    cartCount: PropTypes.number,
    favoriteCount: PropTypes.number
}

Header.defaultProps = {
    cartCount: 0,
    favoriteCount: 0
}

export default Header